local awesomiumdir = "/home/unknown/Development/awesomium"

solution "gm_awesomium_inspector"
    language "C++"
    kind "SharedLib"
    location( _ACTION )
    flags { "Symbols" }
    platforms { "x32" }
    configurations { "Debug" }
    configuration( "Debug" )

project "gm_awesomium_inspector"
    links { "pthread", "dl" } -- needed for dlopen / dlsym

    includedirs {
        awesomiumdir .. "/include/"
    }

    files {
        "./src/**.cpp",
    }

    targetdir( "./bin/" )
