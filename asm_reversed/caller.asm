0xe360f880:    push   ebp
0xe360f881:    mov    ebp, esp
0xe360f883:    push   edi
0xe360f884:    push   esi
0xe360f885:    lea    esi, [ebp+0xc]  // esi = 2nd param dereferenced
0xe360f888:    push   ebx
0xe360f889:    lea    edi, [ebp-0x48] // 18th local?
0xe360f88c:    sub    esp, 0xdc       // add 220 bytes to stack
0xe360f892:    mov    ebx, DWORD PTR [ebp+0x8] // ebx = first param
0xe360f895:    mov    DWORD PTR [esp+0x4], 0x1
0xe360f89d:    mov    DWORD PTR [esp], esi
0xe360f8a0:    call   0xe3618480 // func(this, true)
0xe360f8a5:    mov    DWORD PTR [esp+0x4], 0x1
0xe360f8ad:    mov    DWORD PTR [esp], esi
0xe360f8b0:    call   0xe361fb60 // func2(this, true)
0xe360f8b5:    mov    DWORD PTR [esp+0x4], 0x1
0xe360f8bd:    mov    DWORD PTR [esp], esi
0xe360f8c0:    call   0xe361fe00 // func3(this, true)
0xe360f8c5:    lea    eax, [ebp-0xa9]      // 169th local (local pointer to WebConfig)
0xe360f8cb:    mov    DWORD PTR [esp], eax // dereference WebConfig pointer
0xe360f8ce:    call   0xdba65626 <_ZN9Awesomium9WebConfigC2Ev> // Call WebConfig::WebConfig()
0xe360f8d3:    lea    eax, [ebp-0x1c] // eax = 7th local
0xe360f8d6:    mov    DWORD PTR [esp+0x4], 0xe36330e0 // "--allow-file-access-from-files" .additional_options
0xe360f8de:    mov    DWORD PTR [esp+0x8], eax
0xe360f8e2:    mov    DWORD PTR [esp], edi
0xe360f8e5:    call   0xf73e9960 <_ZNSsC2EPKcRKSaIcE> // std::basic_string
0xe360f8ea:    mov    eax, DWORD PTR [ebp-0x48] // 18th local? (some sort of class)
0xe360f8ed:    lea    esi, [ebp-0x30]           // 12th local (seems to be WebString class)
0xe360f8f0:    mov    edx, DWORD PTR [eax-0xc]  // 3rd dword in eax class
0xe360f8f3:    mov    DWORD PTR [esp+0x4], eax  // 1st arg (const char *data)
0xe360f8f7:    mov    DWORD PTR [esp], esi      // self
0xe360f8fa:    mov    DWORD PTR [esp+0x8], edx  // 2nd arg (unsigned int length)
0xe360f8fe:    call   0xdba665dc <_ZN9Awesomium9WebString14CreateFromUTF8EPKcj> // WebString::CreateFromUTF8
0xe360f903:    lea    eax, [ebp-0x7c]           // 31st local?
0xe360f906:    sub    esp, 0x4                  // Allocate 4 bytes to the stack
0xe360f909:    mov    DWORD PTR [esp+0x4], esi  // WebString class
0xe360f90d:    mov    DWORD PTR [esp], eax      // the created webstring for .additional_options
0xe360f910:    call   0xdba66eee <_ZN9Awesomium14WebStringArray4PushERKNS_9WebStringE> // WebStringArray::Push
0xe360f915:    mov    DWORD PTR [esp], esi      // WebString class
0xe360f918:    call   0xdba66542 <_ZN9Awesomium9WebStringD2Ev> // WebString::~WebString()
0xe360f91d:    mov    eax, DWORD PTR [ebp-0x48]       // 18th local (some sort of class)
0xe360f920:    lea    edx, [eax-0xc]                  // 3rd dword in eax's class (seems to be a webstring)
0xe360f923:    cmp    edx, 0xf74225e0                 // empty string "" _ZNSs4_Rep20_S_empty_rep_storageE
0xe360f929:    jne    0xe360fc12                      // jump to somwhere where ecx will be set to "" ?
0xe360f92f:    lea    eax, [ebp-0x1b]                 // 27 bytes into local
0xe360f932:    mov    DWORD PTR [esp+0x4], 0xe3633016 // "Mozilla/5.0 ("
0xe360f93a:    mov    DWORD PTR [esp+0x8], eax        // set some local to eax
0xe360f93e:    lea    eax, [ebp-0x38]                 // std::basic_string class
0xe360f941:    mov    DWORD PTR [esp], eax            // std::basic_string class
0xe360f944:    call   0xf73e9960 <_ZNSsC2EPKcRKSaIcE> // std::basic_string(eax)
0xe360f949:    lea    eax, [ebp-0x38]                 // std::basic_string class
0xe360f94c:    mov    DWORD PTR [esp+0x8], 0xa        // 10
0xe360f954:    mov    DWORD PTR [esp+0x4], 0xe3633024 // "Linux; X11"
0xe360f95c:    mov    DWORD PTR [esp], eax            // std::basic_string class
0xe360f95f:    call   0xf73e87e0 <_ZNSs6appendEPKcj>  // std::basic_string::append(char*, uint)(eax, char*, 10)
0xe360f964:    lea    eax, [ebp-0x38]                 // std::basic_string class
0xe360f967:    mov    DWORD PTR [esp+0x8], 0x78       // 120
0xe360f96f:    mov    DWORD PTR [esp+0x4], 0xe3633100 // useragent "; Valve Source Client) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1003.1 Safari/535.19 Awesomium/1.7.5.1 GMod/13"
0xe360f977:    mov    DWORD PTR [esp], eax            // std::basic_string class
0xe360f97a:    call   0xf73e87e0 <_ZNSs6appendEPKcj>  // std::basic_string::append(char*, uint)(eax, char*, 120)
0xe360f97f:    mov    edi, DWORD PTR [ebp-0x38] // 14th local (must be a const char*)
0xe360f982:    lea    esi, [ebp-0x2c]           // 11th local (some int)
0xe360f985:    mov    DWORD PTR [esp], edi      // push const char* for strlen
0xe360f988:    call   0xf75c2ca0 <__strlen_sse2_bsf> // strlen, might be getting length of useragent?
0xe360f98d:    mov    DWORD PTR [esp+0x4], edi // 14th local (const char*)
0xe360f991:    mov    DWORD PTR [esp+0x8], eax // std::basic_string or WebString class
0xe360f995:    mov    DWORD PTR [esp], esi     // 11th local (some int)
0xe360f998:    call   0xdba665dc <_ZN9Awesomium9WebString14CreateFromUTF8EPKcj> // WebString::CreateFromUTF8(self, char *data, uint len)
0xe360f99d:    lea    eax, [ebp-0x95] // 149 bytes into local?
0xe360f9a3:    sub    esp, 0x4 // allocate 4 bytes to the stack
0xe360f9a6:    mov    DWORD PTR [esp+0x4], esi // 11th local, some WebString (esi is weird; was an int?, CreateFromUTF8 is NOT editing esi)
0xe360f9aa:    mov    DWORD PTR [esp], eax     // eax is the returned WebString
0xe360f9ad:    call   0xdba665ac <_ZN9Awesomium9WebStringaSERKS0_> // WebString::operator=(WebString, WebString) (copy assignment)
0xe360f9b2:    mov    DWORD PTR [esp], esi // some WebString. Above we just did webconfig.user_agent = above WebString
0xe360f9b5:    call   0xdba66542 <_ZN9Awesomium9WebStringD2Ev> // WebString::~WebString()
0xe360f9ba:    mov    DWORD PTR [ebp-0xa9], 0x2 // local 169/WebConfig = 2 // sets logging level to 2/verbose
0xe360f9c4:    mov    DWORD PTR [esp], 0xe363302f // "Initializing Awesomium..\n"
0xe360f9cb:    call   0xf3220150 <Msg> // Msg(char*, ...)

0xe360f9d0:    lea    eax, [ebp-0xa9]      // $ebp-0xa9 = 0xfff0243f (WebConfig*)
0xe360f9d6:    mov    DWORD PTR [esp], eax // Passes the newly create WebConfig above. Note: method is `static`, no this instance is passed
0xe360f9d9:    call   0xdba656fe <_ZN9Awesomium7WebCore10InitializeERKNS_9WebConfigE> // Awesomium::WebCore::Initialize(WebConfig&)

0xe360f9de:    test   eax, eax
0xe360f9e0:    mov    DWORD PTR [ebx+0x4], eax
0xe360f9e3:    je     0xe360fbd0
0xe360f9e9:    lea    edi, [ebp-0x78]
0xe360f9ec:    mov    DWORD PTR [esp], edi
0xe360f9ef:    call   0xdba661c4 <_ZN9Awesomium14WebPreferencesC2Ev> WebPreferences::WebPreferences()
