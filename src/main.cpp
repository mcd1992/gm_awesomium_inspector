#include <stdio.h>
#include <dlfcn.h>

#include "Awesomium/WebConfig.h"
#include "Awesomium/WebString.h"

#define INITSYMBOL "_ZN9Awesomium7WebCore10InitializeERKNS_9WebConfigE"
#define OSM_EXPORT __attribute__((visibility("default")))

//#pragma pack(push, 1)
namespace Awesomium {
    class OSM_EXPORT WebCore {
    public:
        static WebCore* Initialize(const WebConfig&);
    };

    WebCore* WebCore::Initialize(const WebConfig& config) {
        typedef WebCore* (*initialize_type)(const WebConfig&);
        static initialize_type original_initialize = 0;

        fprintf(stderr, "[fgt] enter detoured WebCore::Initialize\n");
        fprintf(stderr, "[fgt] pid: %i\n", getpid());

        if (!original_initialize) {
            void *awesomium_handle = dlopen("libawesomium-1-7.so.5", RTLD_LAZY);
            original_initialize = (initialize_type)dlsym(awesomium_handle, INITSYMBOL);
            dlclose(awesomium_handle);

            fprintf(stderr, "[fgt] dlsym returned original_initialize: 0x%x\n", original_initialize);

            if (!original_initialize) {
                fprintf(stderr, "[fgt] ERROR: Failed to get original WebCore::Initialize from symbol!\n");
                return NULL;
            }
        }

        const_cast<WebConfig&>(config).remote_debugging_port = 31337;

        WebCore *instance = original_initialize(config);
        fprintf(stderr, "[fgt] original_initialize returned instance: 0x%x\n", instance);

        return instance;
    }
}
//#pragma pack(pop)
